#!/bin/bash
# Call of Cthulhu!

sacrifice() {
    # define your sacrifce to Cthulhu!
    virsh destroy dade-glados
    virsh undefine dade-glados --remove-all-storage --delete-snapshots
}

pray() {
    for ((i=0; i<5; i++)); do
        for _ in 1 2; do [[ $((RANDOM % 100)) -gt 50 ]] && echo -en "\\t"; done
        echo "ph'nglui mglw'nafh Cthulhu R'lyeh wgah'nagl fhtagn!"
        sleep $((1 + RANDOM % 2))
    done
}

pray
echo -e "\\n\\nWe deliver to you this VM, oh Great Old One!\\n";
sacrifice
echo
pray
while IFS="" read -r line || [[ -n "$line" ]]; do
    printf '%s\n' "$line"
    sleep 0.5
done < cthulhu.txt

